# Scrapy settings for webscraper project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#
import os,sys
CURRENT_DIR=os.path.dirname(__file__)
sys.path.append(CURRENT_DIR+'/../../..')
os.environ['DJANGO_SETTINGS_MODULE']='settings'

BOT_NAME = 'Safari'
BOT_VERSION = '535.1'

SPIDER_MODULES = ['webscraper.spiders']
NEWSPIDER_MODULE = 'webscraper.spiders'
DEFAULT_ITEM_CLASS = 'webscraper.items.ProductPageItem'
USER_AGENT = '%s/%s' % (BOT_NAME, BOT_VERSION)
DOWNLOAD_DELAY=2.0
