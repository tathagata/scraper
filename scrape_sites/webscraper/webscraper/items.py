# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field
from scrapy.contrib_exp.djangoitem import DjangoItem
from scrape_sites.models import ProductItem

class ProductItem(DjangoItem):
    django_model = ProductItem

