from scrapy.spider import BaseSpider
import sys
import regex as re
import simplejson as json
from scrapy.http import Request
from xml.sax.saxutils import unescape
from urllib import unquote
import site_settings
from site_settings import Config as site_config
import os
from webscraper.items import ProductItem
class MySpider(BaseSpider):
    name="myspider"
    allowed_domains = []
    start_urls = []
    def __init__(self,name=None,**kwargs):
       self.request_list=[]
    
    def start_requests(self):
        
        
        proj_dir=os.path.dirname(__file__)
        site_config.read(os.path.join(proj_dir,'site_settings.ini'))
        print site_config.sections()
        print site_config
        for item in site_config.sections():
            print 'item is ',item
            value_dict=site_settings.ConfigSectionMap(item)
            print 'dict',str(value_dict)
            self.request_list.append(Request(value_dict.get('url')))
            yield Request(value_dict.get('url'))
            yield
       
        
    
    def parse(self,response):
        print (response.url)
        
        for item in site_config.sections():
            value_dict=site_settings.ConfigSectionMap(item)
            print value_dict.get('url'),value_dict.get('categories')
            if value_dict.get('url')==response.url:
                print 'url is',response.url
                if value_dict.get('categories')=='True':
                    
                    body=response.body
                    
                    categories=re.findall(value_dict.get('category_regex'),body)
                    print 'categories',value_dict.get('category_regex'),categories
                    for item in categories:
                        if value_dict.get('hasparams')=='False':
                            yield (Request(response.url+'/'+item,callback=self.parse_links,meta=value_dict))
                        else:
                            for val in range(1,2000,20):
                                yield (Request(response.url+'/'+item+'&'+value_dict.get('params')\
                            .replace('_',str(val)),callback=self.parse_links,meta=value_dict))
        
               
        return
    def parse_links(self,response):
        print response
        item_regex=response.request.meta.get('item-regex')
        
        source=response.body
        values=re.findall(item_regex,source)
        for item in values:
            print item
            product_item=ProductItem()
            product_item['name']=item[0]
            product_item['price_min']=item[1]
            product_item.save()
        
        
        
   
