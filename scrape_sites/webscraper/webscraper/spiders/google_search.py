from scrapy.spider import BaseSpider
import sys

import simplejson as json
from scrapy.http import Request
from xml.sax.saxutils import unescape
from urllib import unquote
import time

class GoogleSpider(BaseSpider):
    name="googlespider"
    allowed_domains = []
    count=3313
    handle_httpstatus_list = [503]
    start_urls = ['http://www.google.co.in/search?q=0']
    
    
    def parse(self,response):
#		print dir(response)
#		print response.status
		if (str(response.status).startswith('3') or str(response.status).startswith('5')):
			time.sleep(5)
			yield Request('http://www.google.co.in/search?q='+str(GoogleSpider.count))
		if 'did not match any documents' in response.body:
			value= GoogleSpider.count
			print 'value is ',str(value)
		else :
			GoogleSpider.count=GoogleSpider.count+1
			
			yield Request('http://www.google.co.in/search?q='+str(GoogleSpider.count))
        
