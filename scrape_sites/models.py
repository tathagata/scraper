from django.db import models

# Create your models here.
class ProductItem(models.Model):
    name=models.CharField(max_length=150)
    
    price_min=models.CharField(max_length=10)
    
    def __unicode__(self):
        return " | ".join((str(self.id),self.name,self.price))
